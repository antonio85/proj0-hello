Author: Antonio Silva Paucar
E-mail: antonios@uoregon.edu

hello.py will print "Hello world", a message that is stored in credential.ini that is parse by hello.py using "configparser" module. It returns a a dictionary data structure. It is included a .gitignore file that prevent the .ini files to be sent to bitbucket, as well as other types of file that are not needed in the bucket. 
